﻿using System.Web.Mvc;
using GitLabDotNetFramework.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace GitLabDotNetFrameworkUnitTests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Test1()
        {
            // Arrange
            var sut = new HomeController();

            // Act
            var content = sut.About() as ViewResult;

            // Assert
            content.ShouldNotBeNull();
        }
    }
}